var express = require('express');
var router = express.Router();
 const Admin = require('../models/Admin');
const Users= require('../models/Users');
const Lance= require('../models/Lance');
const Hospital= require('../models/Hospital');
const Profile= require('../models/Profile');
const Contacts = require('../models/Contacts');
const networks = require('../models/Networks');
const Teacher= require('../models/Teacher');
const Student= require('../models/Student');
const Comms = require('../models/Comm');
const Class = require('../models/Class');
const Events = require('../models/Event');
const Adverts = require('../models/Advert');
const News = require('../models/News');
const Labs = require('../models/Labs');
const Lesson = require('../models/Lesson');
const Follow = require('../models/Follow');
var uuid = require('node-uuid');
var jwt = require('jsonwebtoken');
var config = require('./config');
const dateTime = require('date-time');
const uniqueRandom = require('unique-random');
const upload = require('./image-upload');
var covresponse = "";
const singleUpload = upload.single('image');
var cors = require('cors')
var nodemailer = require('nodemailer');
const shortid = require('shortid');
const bcrypt = require('bcrypt');
var customId = require("custom-id");
var admin = require('firebase-admin');
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const unirest = require("unirest");
aws.config.update({
    secretAccessKey:'G/18gJWnZnmBvKtM1cY//94oGtWYwPirS/86SbHD',
    accessKeyId: 'AKIAJU35WONEZHFV2WTA',
    region: 'us-east-1' //E.g us-east-1
   });

   const s3 = new aws.S3();

   /* In case you want to validate your file type */
   const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
     cb(null, true);
    } else {
     cb(new Error('Wrong file type, only upload JPEG and/or PNG !'), 
     false);
    }
   };

   const uploadz = multer({
   fileFilter: fileFilter,
   storage: multerS3({
    acl: 'public-read',
    s3,
    bucket: 'rende.com',
    key: function(req, file, cb) {
      /*I'm using Date.now() to make sure my file has a unique name*/
      req.file = Date.now() + file.originalname;
      cb(null, Date.now() + file.originalname);
     }
    })
   });


   router.post('/upload', uploadz.array('image', 1), (req, res) => {
    /* This will be th 8e response sent from the backend to the frontend */

     var location ="https://s3.amazonaws.com/rende.com/"+req.file
    res.send({ image: location,key:req.file });
   });

router.get('/users',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("get users");
    Users.find(req.body).then(function (users) {

        res.status(201).send({users});


    }).catch(next);


});
router.post('/labs',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("get users");
    Labs.create(req.body).then(function (users) {

        res.status(200).send({message: 'sent'});


    }).catch(next);


});

router.post('/addClass',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Class.findOne({ 'username': req.body.username, 'classname': req.body.classname,  }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){
            res.status(200).send({message: 'class exist'});  
          }else{
            
    Class.findOne({ 'username': req.body.username,  }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){



             Class.create(req.body).then(function (users) {

                    res.status(200).send({message: 'sent'});
                
                
          }).catch(next);
            
          }else{
           

            res.status(200).send({message: 'usernotfound'}); 

            
            
          }
      });




            
          }
      });

});
router.post('/rmvClass',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Class.findOne({ 'username': req.body.username, 'classname': req.body.classname,  }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){
            Class.findOneAndDelete(person.classname).then(function (users) {

                res.status(200).send({message: 'ok'});
        
        
            }).catch(next);
          }else{
 

            res.status(200).send({message: 'notfound'});


            
          }
      });

});


router.post('/addLesson',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Teacher.findOne({ 'username': req.body.username }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){
            Lesson.create(req.body).then(function (users) {

                res.status(200).send({message: 'ok'});
        
        
            }).catch(next);
          }else{
 

            res.status(200).send({message: 'noUserfound'});


            
          }
      });

});
router.post('/addStudent',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Student.findOne({ 'username': req.body.username }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){
            res.status(200).send({message: 'username already taken'});
          }else{

            Student.create(req.body).then(function (users) {

                res.status(200).send({message: 'ok'});
        
        
            }).catch(next);
            
          }
      });

});
router.post('/editClass',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Class.findOne({ 'username': req.body.username, 'classname': req.body.classname,  }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){
            Class.findOneAndUpdate(req.body).then(function (users) {

                res.status(200).send({message: 'ok'});
        
        
            }).catch(next);
          }else{
 

            res.status(200).send({message: 'notfound'});


            
          }
      });

});
router.post('/followme',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");

    Class.findOne({  'classname': req.body.classname,  }, function (err, person) {
        if (err) return handleError(err);
       


          if(person){

            Follow.findOne({ 'username': req.body.username, 'classname': req.body.classname,  }, function (err, person) {
                if (err) return handleError(err);
               
        
        
                  if(person){
                    res.status(200).send({message: 'already'});
                  }else{
         
        
                 Follow.create(req.body).then(function (users) {

                res.status(200).send({message: 'ok'});
        
        
             }).catch(next);
        
        
                    
                  }
              });



            
            
          }else{
 

            res.status(200).send({message: 'notfound'});


            
          }
      });

});

router.post('/hosy',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("get users");
    Hospital.create(req.body).then(function (users) {

        res.status(200).send({message: 'sent'});


    }).catch(next);


});
router.post('/getlabs',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
 
    var  maxDistance = req.body.maxDistance;
    var  long =  parseFloat(req.body.long);
    var  lat =   parseFloat(req.body.lat);
    var rank = [long, lat];
  console.log(lat);
  console.log(long);
  console.log(req.body);
    Labs.find({
        location: {
         $near: {
          $maxDistance: parseInt(maxDistance),
          $geometry: {
           type: "Point",
           coordinates: rank
          }
         }
        }
       }).find((error, results) => {
        if (error) console.log(error);
        respond = results;
        //console.log(JSON.stringify(results, 0, 2));
        res.status(200).send({message: respond});
       });

      
 
});
router.post('/gethosy',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
 
    var  maxDistance = req.body.maxDistance;
    var  long =  parseFloat(req.body.long);
    var  lat =   parseFloat(req.body.lat);
    var rank = [long, lat];
  Hospital.find({
        location: {
         $near: {
          $maxDistance: parseInt(maxDistance),
          $geometry: {
           type: "Point",
           coordinates: rank
          }
         }
        }
       }).find((error, results) => {
        if (error) console.log(error);
        respond = results;
        //console.log(JSON.stringify(results, 0, 2));
        res.status(200).send({message: respond});
       });

      
 
});

router.post('/bycountry',function (req,ress,next) {
 
    var name = req.body.name;
    var req = unirest("GET", "https://covid19-data.p.rapidapi.com/");

    req.query({
        "iso3": name
    });
    
    req.headers({
        "x-rapidapi-host": "covid19-data.p.rapidapi.com",
        "x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
    });
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
    
       // console.log(res.body);
       // covresponse = JSON.stringify()
        ress.status(200).send(res.body);
    });
  

});
router.post('/total',function (req,ress,next) {
 
    var req = unirest("GET", "https://coronavirus-monitor.p.rapidapi.com/coronavirus/worldstat.php");

    req.headers({
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
    });
    
    
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
    
    
    });
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
    
       // console.log(res.body);
       // covresponse = JSON.stringify()
        ress.status(200).send(res.body);
    });
  

});

router.post('/gp',function (req,ress,next) {
    var name = req.body.name;
    //var name = req.body.name;
 
    var req = unirest("GET", "https://covid19-data.p.rapidapi.com/");

    req.query({
        "iso3": "ZA"
    });
    
    req.headers({
        "x-rapidapi-host": "covid19-data.p.rapidapi.com",
        "x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
    });
    
    
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
    
        console.log(res.body);
    });
    
    
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
    
        ress.status(200).send(res.body);
    });
     
    


        
    });
      



router.post('/all',function (req,ress,next) {
    var name = req.body.name;
    //var name = req.body.name;
    var req = unirest("GET", "https://restcountries-v1.p.rapidapi.com/all");

    req.headers({
        "x-rapidapi-host": "restcountries-v1.p.rapidapi.com",
        "x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
    });
    req.end(function (res) {
        if (res.error) throw new Error(res.error);
        var data = new Array();
        data = res.body;
        // var code = data.filter(function (code) {
        //     return code.name === "Afghanistan";
        //   });
     
        //   var jediPersonnel = data.filter(function (c) {
        //     return c.name === "Afghanistan";
        //   });

          
       // covresponse = JSON.stringify()
       //var skinName = data.find(x=>x.alpha3Code == name).alpha2Code;
       //const filtered = data.filter(r => r.alpha3Code === name)
       const found = data.find(r => r.name === name)
     
       var req = unirest("GET", "https://covid19-data.p.rapidapi.com/");

       req.query({
           "iso3": found.alpha3Code
       });
       
       req.headers({
           "x-rapidapi-host": "covid19-data.p.rapidapi.com",
           "x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
       });
       req.end(function (res) {
           if (res.error) throw new Error(res.error);
       
          // console.log(res.body);
          // covresponse = JSON.stringify()
           ress.status(200).send(res.body);
       });
     



        
    });
      
    });
  
    router.post('/gs',function (req,ress,next) {
        var name = req.body.name;
        //var name = req.body.name;
       console.log(name);
        var req = unirest("GET", "https://covid19-data.p.rapidapi.com/");

req.query({
	"iso2": name
});

req.headers({
	"x-rapidapi-host": "covid19-data.p.rapidapi.com",
	"x-rapidapi-key": "fd94c1d319mshfa733740fe670afp1705e9jsnfba7d65021a4"
});


req.end(function (res) {
	if (res.error) throw new Error(res.error);

    ress.status(200).send(res.body);
});
         
    
    
    
            
        


    });

router.post('/adverts',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    Adverts.create(req.body).then(function (Admin) {

        res.status(201).send({message:'ok'});
 
     }).catch(next);


});
router.get('/allevents',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    Adverts.find().then(function (Admin) {

        res.status(201).send({Admin});
 
     }).catch(next);


});

router.get('/geteventype',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    Events.find().then(function (events) {

        res.status(201).send({events});
 
     }).catch(next);


});
router.post('/getnews',function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("get users");

    News.find(req.body).then(function (users) {

        res.status(201).send({users});


    }).catch(next);


});

router.post('/reset',  cors(),function (req,res,next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
     var email ={
         email:req.body.email,
     }
  
     Users.findOne(email).then(function (Admin) {
        if(Admin){
          
            var codez = customId({});
            var update ={
                _id : Admin._id,
                code: codez
            }







            console.log(update);
            Users.findOneAndUpdate(update).then(function (users) {
    
        res.status(200).send({message: 'sent'});




              var transporter = nodemailer.createTransport({
            service: 'gmail',
           auth: {
                 user: 'royalezbby@gmail.com',
                 pass: 'munashe95'
             }
         });
       
         var mailOptions = {
             from: 'royalezbby@gmail.com',
             to: req.body.email,
             subject: 'Support Rende',
             text: 'Password Reset ',
             html: '<h1>  Your password Reset code is '+update.code+' '+
             ''
       
         };
       
         transporter.sendMail(mailOptions, function(error, info){
             if (error) {
                 console.log(error);
             } else {
                 console.log('Email sent: ' + info.response);
             }
         }); 

    }).catch(next);


           
        }else{
            res.status(200).send({message:'notfound'});
        }

        

        


    }).catch(next);




    // Users.create(req.body).then(function (Admin) {

    //     res.status(201).send({message:'created'});

    // }).catch(next);

    

});
router.post('/pwd',  cors(),function (req,res,next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    var email ={
        email:req.body.email,
        code: req.body.code
    }
    Users.findOne(email).then(function (Admin) {
        var password = req.body.password;
   
        let hash = bcrypt.hashSync(password, 10);
  


        if(Admin){
            var found ={
                username:Admin.username,
                password: hash
                
            }

            Users.findOneAndUpdate(found).then(function (users) {

         
        
        
            }).catch(next);
            res.status(200).send({message:'found'});

            
        }else{
            res.status(200).send({message:'not found'});
        }

        

        


    }).catch(next);


});


router.post('/status',  cors(),function (req,res,next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    networks.findOne(req.body).then(function (Admin) {
        if(Admin){
            res.status(200).send({message:Admin.status,net:Admin.netype});
        }else{
            res.status(200).send({message:'not found'});
        }

        

        


    }).catch(next);

});






router.post('/addteacher',cors(),function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    Teacher.findOne({
        email: req.body.email
    }, function(err, user) {

        if(user){  
            res.status(200).send({message:'Teacher name  you entered is already taken'});

           
        }else {

            Teacher.findOne({
                username: req.body.username
            }, function(err, user) {
        
                if(user){  
                    res.status(200).send({message:'The username you have entered is already associated with another account.'});
        
                   
                }else {
        
                    Teacher.create(req.body).then(function (Admin) {

                                        res.status(201).send({message:'created'});
                        
                             var transporter = nodemailer.createTransport({
                     service: 'gmail',
                    auth: {
                          user: 'royalezbby@gmail.com',
                          pass: 'munashe95'
                      }
                  });
                
                  var mailOptions = {
                      from: 'royalezbby@gmail.com',
                      to: req.body.email,
                      subject: 'Support Guru',
                      text: 'Thanks Welcome to Guru ',
                      html: '<h1>  congratulations '+req.body.username+'You have registered  '+
                      '<a href="http://technotto.com/test@netmzansi/networks.html" target="_blank">Visit Netmzansi!</a>'
                
                  };
                
                  transporter.sendMail(mailOptions, function(error, info){
                      if (error) {
                          console.log(error);
                      } else {
                          console.log('Email sent: ' + info.response);
                      }
                  });        
                                 
                         }).catch(next);
        
        
           
        
                }
        
        
            }).catch(next);


   

        }


    }).catch(next);
});

router.post('/register',cors(),function (req,res,next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    Users.findOne({
        email: req.body.email
    }, function(err, user) {

        if(user){  
            res.status(200).send({message:'email you entered is already taken'});

           
        }else {

            Users.findOne({
                username: req.body.username
            }, function(err, user) {
        
                if(user){  
                    res.status(200).send({message:'The username you have entered is already associated with another account.'});
        
                   
                }else {
        
                       Users.create(req.body).then(function (Admin) {

                                        res.status(201).send({message:'created'});
                        
                             var transporter = nodemailer.createTransport({
                     service: 'gmail',
                    auth: {
                          user: 'royalezbby@gmail.com',
                          pass: 'munashe95'
                      }
                  });
                
                  var mailOptions = {
                      from: 'royalezbby@gmail.com',
                      to: req.body.email,
                      subject: 'Support Guru',
                      text: 'Thanks Welcome to Guru ',
                      html: '<h1>  congratulations '+req.body.username+'You have registered  '+
                      '<a href="http://technotto.com/test@netmzansi/networks.html" target="_blank">Visit Netmzansi!</a>'
                
                  };
                
                  transporter.sendMail(mailOptions, function(error, info){
                      if (error) {
                          console.log(error);
                      } else {
                          console.log('Email sent: ' + info.response);
                      }
                  });        
                                 
                         }).catch(next);
        
        
           
        
                }
        
        
            }).catch(next);


   

        }


    }).catch(next);
});




router.post('/loginss',function (req,res,next) {


    Users.findOne({
        username: req.body.username
    }, function(err, user) {

        console.log(err);

        if(!user){


            res.status(401).send({message:'wrong username or password'});
        }else {

            // if(user.password.toString()=== req.body.password.toString()){
            //     res.send(user);
            // }else {
            //     res.status(401).send({message:'wrong username or password'});
            // }
            user.comparePassword(req.body.password, function(err, isMatch) {
                if (err) throw err;
                console.log('correct', isMatch); // -> Password123: true

                if(isMatch==true){



                    var token = jwt.sign({ user: user.username }, config.secret, {
                        expiresIn: 86400 // expires in 24 hours
                    });

                    res.status(200).send({ auth: true, token: token });
                    // res.status(200).send(user);
                }else{
                    res.status(401).send({message:'wrong username or password'});
                }
            });


        }


    }).catch(next);
});
router.post('/teacherCheck',function (req,res,next) {


    Teacher.findOne({
        username: req.body.username
    }, function(err, user) {

        console.log(err);

        if(!user){


            res.status(401).send({message:'wrong username or password'});
        }else {

            // if(user.password.toString()=== req.body.password.toString()){
            //     res.send(user);
            // }else {
            //     res.status(401).send({message:'wrong username or password'});
            // }
            user.comparePassword(req.body.password, function(err, isMatch) {
                if (err) throw err;
                console.log('correct', isMatch); // -> Password123: true

                if(isMatch==true){



                    var token = jwt.sign({ user: user.username }, config.secret, {
                        expiresIn: 86400 // expires in 24 hours
                    });

                    res.status(200).send({ auth: true, token: token });
                    // res.status(200).send(user);
                }else{
                    res.status(401).send({message:'wrong username or password'});
                }
            });


        }


    }).catch(next);
});

router.post('/studentCheck',function (req,res,next) {


    Student.findOne({
        username: req.body.username
    }, function(err, user) {

        console.log(err);

        if(!user){


            res.status(401).send({message:'wrong username or password'});
        }else {

            // if(user.password.toString()=== req.body.password.toString()){
            //     res.send(user);
            // }else {
            //     res.status(401).send({message:'wrong username or password'});
            // }
            user.comparePassword(req.body.password, function(err, isMatch) {
                if (err) throw err;
                console.log('correct', isMatch); // -> Password123: true

                if(isMatch==true){



                    var token = jwt.sign({ user: user.username }, config.secret, {
                        expiresIn: 86400 // expires in 24 hours
                    });


                    Follow.find({ username: req.body.username}).then(function (events) {

                        res.status(200).send({classlist:events, surname:user.surname,name:user.name});
                 
                     }).catch(next);
              
                   
                    // res.status(200).send(user);
                }else{
                    res.status(401).send({message:'wrong username or password'});
                }
            });


        }


    }).catch(next);
});


router.post('/pf',function (req,res,next) {


    Profile.findOne({
        name: req.body.name
    }, function(err, user) {



        if(user){
      

            res.status(401).send({message:'exist'});
        }else {

          Profile.create(req.body).then(function (Admin) {

       res.status(201).send({message:'ok'});

    }).catch(next);
            
            


        }


    }).catch(next);
});
router.post('/getpf',function (req,res,next) {

console.log(req.body.user);
    Profile.find({
        username: req.body.username
    }, function(err, user) {



        if(user){
      

            res.status(200).send({user});
        }else {


            res.status(201).send({message:'noprofile'});
            
            


        }


    }).catch(next);
});
router.post('/pfupdate',function (req,res,next) {


    Profile.findOne({
        username: req.body.username
    }, function(err, user) {
  const key = user .key;
  const username = user.username;
  

        if(user){
            console.log(user);
            aws.config.update({
                secretAccessKey:'G/18gJWnZnmBvKtM1cY//94oGtWYwPirS/86SbHD',
                accessKeyId: 'AKIAJU35WONEZHFV2WTA',
                region: 'us-east-1' //E.g us-east-1
               });
               const s3 = new aws.S3();

            var params = {
              Bucket: 'rende.com', 
              Key: key
            };
            
            s3.deleteObject(params, function(err, data) {
                if (err) console.log(err)     
                else console.log("Successfully deleted myBucket/myKey");  
                

                   if(username === req.body.username){
                    Profile.findOneAndUpdate(req.body).then(function (users) {

         
        
        
                    }).catch(next);
                    res.status(200).send({message:'ok'});


                   }else{
                    res.status(401).send({message:'mismatch username'});  
                   }
                
            });
          
         
        
     

           // res.status(401).send({message:'exist'});
        }else {

    res.status(401).send({message:'NOTFOUND'});
            
            


        }


    }).catch(next);
});
//  var token = req.headers['x-access-token'];
//if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
module.exports = router;

