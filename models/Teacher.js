const mongoose = require('mongoose');
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
//create user schema

const  TeacherSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'email field is required']
    },
    email:{
        type:String,
        unique:true,
        required:[true,'email field is required']
    },
    school:{
        type:String,
        required:[true,'gcm field is required']
    },
    code:{
        type:String,
        required:[false,'code field is required']
    },
    password:{
        type:String,
        required:[true,'password field is required']
    },
    hash : String, 
    salt : String 
});


TeacherSchema.pre('save', function(next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();
    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
     if (err) return next(err);
     // hash the password using our new salt
     bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
     });
    });
   });

   
   TeacherSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
     if (err) return cb(err);
     cb(null, isMatch);
    });
   };
 




const Teacher = mongoose.model('Teacher',TeacherSchema);
module.exports=Teacher;