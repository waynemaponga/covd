const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  AdvertSchema = new Schema({
    username:{
        type:String,
        required:true,
        required:['username field is required']

    },
    type:{
        type:String,
        required:['type field is required']

    },
    name:{
        type:String,
        required:true,
        required:['name field is required']

    },

    key:{
        type:String,
        required:true,
        required:['name field is required']

    },
    date:{
        type:Date,
        required:true,
        required:['name field is required']

    },
    image:{
        type:Date,
        required:true,
        required:['image field is required']

    },
    video:{
        type:String,
     
        required:['video field is required']

    },
    price:{
        type:String,
        required:true,
        required:['price field is required']

    },
    location:{
        type:String,
        required:true,
        required:['location field is required']

    },

});



const Adverts = mongoose.model('adverts',AdvertSchema);
module.exports= Adverts;