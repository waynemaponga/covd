const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LabsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    number: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    location: {
        type: [Number],
        index: "2d"
    }
}, {
    timestamps: {
        createdAt: "created_at",
        updatedAt: "updated_at",
        required: false
    }
    
});
LabsSchema.index({ location: "2dsphere" });
const Labs = mongoose.model('Labs',LabsSchema);
module.exports=Labs;