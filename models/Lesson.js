const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const LessonSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    classname: {
        type: String,
        required: true
    },
    note: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
        
    },
    question: {
        type: String,
        required: true
    }}, {
        timestamps: {
            createdAt: "created_at",
            updatedAt: "updated_at",
            required: false
        }
    
});

const Lesson = mongoose.model('Lesson',LessonSchema);
module.exports=Lesson;