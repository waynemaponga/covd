const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  ClassSchema = new Schema({
    classname:{
        type:String,
        unique:true,
        required:['Name field is required']

    },
    username:{
        type:String,
        required:['username field is required']

    },

});



const Class = mongoose.model('Class',ClassSchema);
module.exports=Class;